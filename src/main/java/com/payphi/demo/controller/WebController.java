package com.payphi.demo.controller;

import com.payphi.demo.model.RedirectionDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

@Controller
@RequestMapping("/web")
@Slf4j
public class WebController {

    @PostMapping(value = "/processRedirectResponse", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public String processRedirectResponse(Model model, @RequestParam HashMap<String, String> paramMap,
                                          HttpServletResponse response) throws Exception {

        for (String key : paramMap.keySet()) {
            System.out.println("entry --- > " + key + " : " + paramMap.get(key));
        }
        // Save feedback data
        //return new ResponseEntity<String>("Thank you for submitting feedback", HttpStatus.OK);
        //sendRedirect("https://qa.phicommerce.com/pg/api/merchant");

        RedirectionDetails redirectionDetails = new RedirectionDetails("https://qa.phicommerce.com/pg/api/merchant", "POST", paramMap);

        model.addAttribute("redirectionDetails", redirectionDetails);
        log.info("redirect: redirectionDetails - {}", redirectionDetails);
        log.info("redirect: END");

        return "redirect";

    }
}
