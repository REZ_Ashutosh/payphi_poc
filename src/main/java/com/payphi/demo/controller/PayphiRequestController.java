package com.payphi.demo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.payphi.demo.entity.OrderEntity;
import com.payphi.demo.entity.OrderStatusEntity;
import com.payphi.demo.service.PayphiService;
import com.payphi.demo.service.SecureHashService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api/v1/payphi")
@Slf4j
public class PayphiRequestController {

    @Autowired
    private PayphiService payphiService;

    @Autowired
    private SecureHashService secureHashService;

    @PostMapping("/create")
    public ResponseEntity<String> createOrder(@RequestBody OrderEntity payPhiOrderData) throws IOException, URISyntaxException {

        String response = payphiService.createOrder(payPhiOrderData);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/secureHashForString")
    public String secureHashString(@RequestParam("msg") String msg,
                                   @RequestParam("key") String key) throws JsonProcessingException {
        return secureHashService.getSecureHashFromString(msg, key);
    }

    @GetMapping("/secureHashForJSON")
    public String secureHashString(@RequestBody JsonNode jsonNode, @RequestParam("key") String key) throws JsonProcessingException {

        String msgJsonStr = jsonNode.toString();
        return secureHashService.getSecureHashFromJSON(msgJsonStr, key);
    }

    @PostMapping("/status")
    public ResponseEntity<String> OrderStatus(@RequestBody OrderStatusEntity orderStatusEntity) throws IOException, URISyntaxException {

        String response = payphiService.getOrderStatus(orderStatusEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/processWebhookResponse")
    public String processWebhookResponse(@RequestBody JsonNode jsonNode) throws JsonProcessingException {

        String processWebhookStr = jsonNode.toString();
        System.out.println("----->>>> " + processWebhookStr);
        return processWebhookStr;
    }
}
