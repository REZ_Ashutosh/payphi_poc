package com.payphi.demo.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.TreeMap;

@Service
@Slf4j
public class SecureHashService {

    private String hmacDigest(String msg, String keyString) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(key);
            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));
            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) hash.append('0');
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException e) {
        } catch (InvalidKeyException e) {
        } catch (NoSuchAlgorithmException e) {
        }
        return digest;
    }

    public String getSecureHashFromJSON(String jsonString, String key) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        TreeMap<String, String> stringMap = objectMapper.readValue(
                jsonString, new TypeReference<TreeMap<String, String>>() {});

        String msg = "";
        for (Map.Entry<String, String> entry : stringMap.entrySet()) {
            if(!"secureHash".equalsIgnoreCase(entry.getKey())) {
                msg = msg + entry.getValue();
            }
        }
        log.info("hashtext >>> " + msg);

        String secureHash  =  getSecureHashFromString(msg, key);

        log.info("secureHash >>> " + secureHash);

        return secureHash;
    }

    public String getSecureHashFromString(String msg, String key) throws JsonProcessingException {

        String secureHash  =  hmacDigest(msg, key);

        log.info("secureHash >>> " + secureHash);

        return secureHash;
    }
}
