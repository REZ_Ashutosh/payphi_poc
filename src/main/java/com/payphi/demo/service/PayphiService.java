package com.payphi.demo.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.payphi.demo.entity.OrderEntity;
import com.payphi.demo.entity.OrderStatusEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.net.*;
import java.util.Map;
import java.util.TreeMap;

@Service
@Slf4j
public class PayphiService {
    @Autowired
    private SecureHashService secureHashService;

    public String createOrder(OrderEntity payPhiOrderData) throws IOException, URISyntaxException {

        ResponseEntity<String> result;
        try {
            ObjectWriter ow = new ObjectMapper().writer();
            String jsonOrderString = ow.writeValueAsString(payPhiOrderData);

            String secureHash = secureHashService.getSecureHashFromJSON(jsonOrderString, "abc");

            payPhiOrderData.setSecureHash(secureHash);

            log.info(secureHash);

            String url = "https://qa.phicommerce.com/pg/api/v2/initiateSale";

            RestTemplate restTemplate = new RestTemplate();
            URI uri = new URI(url);

            result = restTemplate.postForEntity(uri, payPhiOrderData, String.class);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return result.getBody();
    }

    public String getOrderStatus(OrderStatusEntity orderStatusEntity) throws IOException, URISyntaxException {

        URL url = null;
        InputStream stream = null;
        HttpURLConnection urlConnection = null;
        try {
            ObjectWriter ow = new ObjectMapper().writer();
            String jsonOrderString = ow.writeValueAsString(orderStatusEntity);

            String secureHash = secureHashService.getSecureHashFromJSON(jsonOrderString, "abc");

            orderStatusEntity.setSecureHash(secureHash);

            url = new URL("https://qa.phicommerce.com/pg/api/command");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);

            ObjectMapper objectMapper = new ObjectMapper();
            TreeMap<String, String> stringMap = objectMapper.readValue(jsonOrderString, new TypeReference<TreeMap<String, String>>() {});

            String msg = "";
            int i = 0;

            for (Map.Entry<String, String> entry : stringMap.entrySet()) {
                if(i>0) {
                    msg += "&";
                }
                msg += URLEncoder.encode(entry.getKey(), "UTF-8") + "=" + URLEncoder.encode(entry.getValue(), "UTF-8");

                i++;
            }

            urlConnection.connect();

            OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
            wr.write(msg);
            wr.flush();

            stream = urlConnection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"), 8);
            String result = reader.readLine();
            return result;

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
//            Log.i("Result", "SLEEP ERROR");
        }
        return null;
    }

}
