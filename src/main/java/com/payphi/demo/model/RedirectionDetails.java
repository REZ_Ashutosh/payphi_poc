package com.payphi.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RedirectionDetails {
    private String url;
    private String method;
    private Map<String,String> params;
}