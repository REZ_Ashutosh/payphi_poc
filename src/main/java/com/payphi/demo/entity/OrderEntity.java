package com.payphi.demo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderEntity {
    @JsonProperty("merchantId")
    String merchantId;

    @JsonProperty("merchantTxnNo")
    String merchantTxnNo;

    @JsonProperty("amount")
    String amount;

    @JsonProperty("currencyCode")
    String currencyCode;

    @JsonProperty("payType")
    String payType;

    @JsonProperty("customerEmailID")
    String customerEmailID;

    @JsonProperty("transactionType")
    String transactionType;

    @JsonProperty("txnDate")
    String txnDate;

    @JsonProperty("aggregatorID")
    String aggregatorID;

    @JsonProperty("returnURL")
    String returnURL;

    @JsonProperty("secureHash")
    String secureHash;
}
