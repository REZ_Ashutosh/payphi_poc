package com.payphi.demo.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderStatusEntity {

    @JsonProperty("merchantID")
    String merchantID;

    @JsonProperty("merchantTxnNo")
    String merchantTxnNo;

    @JsonProperty("originalTxnNo")
    String originalTxnNo;

    @JsonProperty("transactionType")
    String transactionType;

    @JsonProperty("secureHash")
    String secureHash;

    @JsonProperty("aggregatorID")
    String aggregatorID;
}
