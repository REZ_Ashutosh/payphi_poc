FROM maven:3.6.3-jdk-11 AS build
WORKDIR .
COPY src src
COPY pom.xml pom.xml
RUN mvn clean install



FROM openjdk:11-jre
EXPOSE 8081
WORKDIR .
COPY --from=build /target/payphi_poc-*.jar ./payphi_poc.jar
CMD ["java", "-jar", "./payphi_poc.jar"]