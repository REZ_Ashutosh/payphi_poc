import { Routes, Route } from "react-router-dom";
import DemoPage from "./component/DemoSourcePage/DemoPage";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<DemoPage />} />
      </Routes>
    </>
  );
}

export default App;
