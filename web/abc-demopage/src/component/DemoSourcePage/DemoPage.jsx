import React, { useState } from "react";
import "./style.scss";
import ab_logo from "../img/ab_logo.png";
import { initialState } from "./InitialState";
import { Createtranx } from "../../Service";

const DemoPage = () => {
  const [state, setState] = useState(initialState);

  const onInputChange = (e) => {
    setState((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };

  const redirectToUrl  =()=>{
    // window.open("https://www.youtube.com/","_blank");
    Createtranx(state)
  }

  return (
    <>
      <div className="header-wrapper">
        <div className="header">
          <div className="contain">
            <div className="logo">
              <img src={ab_logo} />
            </div>
            <div>
              <h1 className="page-heading">Demo Source Page</h1>
            </div>
            <div></div>
          </div>
        </div>
      </div>

      <div className="contain-wrapper">
        <div className="row">
          <div className="col-8">
            <div className="root-container">
              <div className="row">
                <div className="col-6">
                  <div class="form-floating ">
                    <input
                      type="text"
                      class="form-control mb-2"
                      name="merc_order_id"
                      autocomplete="off"
                      id="merc_order_id"
                      placeholder="Merchant Order ID"
                      value={state.merc_order_id}
                      onChange={onInputChange}
                    />
                    <label for="floatingInput">Merchant Order ID</label>
                  </div>
                </div>
                <div className="col-6">
                  <div class="form-floating ">
                    <input
                      name="merc_order_date"
                      type="text"
                      class="form-control mb-2"
                      id="merc_order_date"
                      placeholder="Merchant Order Date"
                      value={state.merc_order_date}
                      onChange={onInputChange}
                    />
                    <label for="floatingInput">Merchant Order Date</label>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-6">
                  <div class="form-floating ">
                    <input
                      type="text"
                      class="form-control mb-2"
                      name="amount"
                      autocomplete="off"
                      id="amount"
                      placeholder="Amount"
                      value={state.amount}
                      onChange={onInputChange}
                    />
                    <label for="floatingInput">Amount</label>
                  </div>
                </div>
                <div className="col-6">
                  <div class="form-floating ">
                    <input
                      type="text"
                      name="merchant_id"
                      class="form-control mb-2"
                      id="merchant_id"
                      placeholder="Merchant ID"
                      autocomplete="off"
                      value={state.merchant_id}
                      onChange={onInputChange}
                    />
                    <label for="floatingInput">Merchant ID</label>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-6">
                  <div class="form-floating ">
                    <input
                      type="text"
                      class="form-control mb-2"
                      name="product_type"
                      autocomplete="off"
                      id="product_type"
                      placeholder="Product Type"
                      value={state.product_type}
                      onChange={onInputChange}
                    />
                    <label for="floatingInput">Product Type</label>
                  </div>
                </div>
                <div className="col-6">
                  <div class="form-floating ">
                    <input
                      type="text"
                      name="returnURL"
                      class="form-control mb-2"
                      id="returnURL"
                      placeholder="RETURN URL"
                      autocomplete="off"
                      value={state.returnURL}
                      onChange={onInputChange}
                    />
                    <label for="floatingInput">RETURN URL</label>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-6">
                  <div class="form-floating ">
                    <input
                      type="text"
                      class="form-control mb-2"
                      name="customer_id"
                      autocomplete="off"
                      id="customer_id"
                      placeholder="Customer ID"
                      value={state.customer_id}
                      onChange={onInputChange}
                    />
                    <label for="floatingInput">Customer ID</label>
                  </div>
                </div>
                <div className="col-6">
                  <div class="form-floating ">
                    <input
                      type="text"
                      name="invoice_No"
                      class="form-control mb-2"
                      id="invoice_No"
                      placeholder="Invoice No"
                      autocomplete="off"
                      value={state.invoice_No}
                      onChange={onInputChange}
                    />
                    <label for="floatingInput">Invoice No</label>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-6">
                  <div class="form-floating ">
                    <input
                      type="text"
                      class="form-control mb-2"
                      name="customer_name"
                      autocomplete="off"
                      id="customer_name"
                      placeholder="Customer Name "
                      value={state.customer_name}
                      onChange={onInputChange}
                    />
                    <label for="floatingInput">CUSTOMER NAME</label>
                  </div>
                </div>
                <div className="col-6">
                  <div class="form-floating ">
                    <input
                      type="text"
                      name="customer_email"
                      class="form-control mb-2"
                      id="customer_email"
                      placeholder="Customer Email"
                      autocomplete="off"
                      value={state.customer_email}
                      onChange={onInputChange}
                    />
                    <label for="floatingInput">CUSTOMER EMAIL</label>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-6">
                  <div class="form-floating">
                    <input
                      type="text"
                      class="form-control mb-2"
                      name="customer_phone"
                      autocomplete="off"
                      id="customer_phone"
                      placeholder="Customer Phone"
                      value={state.customer_phone}
                      onChange={onInputChange}
                    />
                    <label for="floatingInput">Customer Phone</label>
                  </div>
                </div>
                <div className="col-6">
                  <div class="form-floating">
                    <input
                      type="text"
                      class="form-control mb-2"
                      name="order_Desc"
                      autocomplete="off"
                      id="order_Desc"
                      placeholder="Order Description"
                      value={state.order_Desc}
                      onChange={onInputChange}
                    />
                    <label for="floatingInput">Order Description</label>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-6">
                  <div class="form-floating ">
                    <input
                      type="text"
                      class="form-control mb-2"
                      name="udf1"
                      autocomplete="off"
                      id="udf1"
                      placeholder="UDF1 "
                      value={state.udf1}
                      onChange={onInputChange}
                    />
                    <label for="floatingInput">UDF1</label>
                  </div>
                </div>
                <div className="col-6">
                  <div class="form-floating ">
                    <input
                      type="text"
                      class="form-control mb-2"
                      name="udf2"
                      autocomplete="off"
                      id="udf2"
                      placeholder="UDF2 "
                      value={state.udf2}
                      onChange={onInputChange}
                    />
                    <label for="floatingInput">UDF2</label>
                  </div>
                </div>
              </div>
              {/* <div className="row">
                <div className="col-6">
                  <div class="form-floating ">
                    <input
                      type="text"
                      class="form-control mb-2"
                      name="view_type"
                      autocomplete="off"
                      id="view_type"
                      placeholder="View Type "
                      value={state.view_type}
                      onChange={onInputChange}
                    />
                    <label for="floatingInput">View Type</label>
                  </div>
                </div>
              </div> */}
            </div>
          </div>
          <div className="col-4">
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingOne">
                <button
                  class="accordion-button"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#collapseOne"
                  aria-expanded="true"
                  aria-controls="collapseOne"
                >
                  Request JSON
                </button>
              </h2>
              <div
                id="collapseOne"
                class={`accordion-collapse collapse show `}
                aria-labelledby="headingOne"
                data-bs-parent="#accordionExample"
              >
                <div class="accordion-body reqjsonAcc">
                  <div class="form-check form-switch">
                    <label
                      class="form-check-label"
                      for="flexSwitchCheckDefault"
                    >
                      Redirect
                    </label>
                    <input
                      class="form-control form-check-input"
                      type="checkbox"
                      id="flexSwitchCheckDefault"
                      value="true"
                    />
                  </div>
                  <div class="">
                    <textarea
                      id="reqresfields"
                      col="40"
                      row="18"
                      className="text_area"
                      value={JSON.stringify(state, undefined, 4)}
                    ></textarea>
                    <div class="footer">
                      <button
                        class="submit btn btn-md btn-success"
                        onClick={redirectToUrl}
                      >
                        CREATE ORDER <i class="bi bi-arrow-right-circle"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingTwo">
                <button
                  class="accordion-button collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#collapseTwo"
                  aria-expanded="true"
                  aria-controls="collapseTwo"
                >
                  Response JSON
                </button>
              </h2>
              <div
                id="collapseTwo"
                class={`accordion-collapse collapse `}
                aria-labelledby="headingTwo"
                data-bs-parent="#accordionExample"
              >
                <div class="accordion-body">
                  <div class="">
                    <textarea
                      id="reqresfields"
                      cols="40"
                      rows="18"
                      readonly=""
                    ></textarea>
                    <div class="footer">
                      <button class="btn btn-md text-primary">
                        <i class="bi-files"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default DemoPage;
